import React, { Component } from "react";
import Moment from "moment";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity
} from "react-native";

import { connect } from "react-redux";
import { getNews } from "../../store/actions/news_actions";

class NewsComponent extends Component {
  componentDidMount() {
    this.props.dispatch(getNews());
  }

  renderArticle = news =>
    news.articles
      ? news.articles.map((item, i) => (
          <TouchableOpacity
            key={i}
            onPress={() =>
              this.props.navigation.navigate("Article", {
                ...item
              })
            }
          >
            <View style={styles.cardContainer}>
              <View>
                <Image
                  style={{ height: 150, justifyContent: "space-around" }}
                  source={{ uri: `${item.image}` }}
                  resizeMode="cover"
                />
              </View>
              <View style={styles.cardContent}>
                <Text style={styles.cardTitle}>{item.title}</Text>
                <View style={styles.cardBottom}>
                  <Text style={styles.cardBottomTeam}>{item.team} - </Text>
                  <Text style={styles.cardBottomDate}>
                    Posted At {Moment(item.date).format("d MMMM")}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        ))
      : null;

  render() {
    return (
      <ScrollView style={{ backgroundColor: "#f0f0f0" }}>
        {this.renderArticle(this.props.News)}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: "#ffffff",
    margin: 10,
    shadowColor: "#dddddd",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 2
  },
  cardContent: {
    borderWidth: 1,
    borderColor: "#dddddd"
  },
  cardTitle: {
    fontFamily: "Roboto-Bold",
    color: "#232323",
    fontSize: 16,
    padding: 10
  },
  cardBottom: {
    flex: 1,
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: "#e6e6e6",
    padding: 10
  },
  cardBottomTeam: {
    fontFamily: "Roboto-Bold",
    color: "#828282",
    fontSize: 12
  },
  cardBottomDate: {
    fontFamily: "Roboto-Light",
    color: "#828282",
    fontSize: 12
  }
});

function mapStateToProps(state) {
  console.log(state);
  return {
    News: state.News
  };
}

export default connect(mapStateToProps)(NewsComponent);
