import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  Image,
  ImageBackground
} from "react-native";

import Nav from "./src/nav";
import Generator from "./src/generator";
import ListItem from "./src/listitem";
import Input from "./src/input";
import Picker from "./src/picker";
import Mountain from "./src/assets/images/mountain.jpeg";
import ModalComponet from "./src/modal";

class App extends Component {
  state = {
    nameOfApp: "My Awesome App",
    random: [20, 434]
  };

  onAddRandom = () => {
    const random = Math.floor(Math.random() * 100) + 1;
    this.setState(prevState => {
      return {
        random: [...prevState.random, random]
      };
    });
  };

  onItemDelete = position => {
    const newArray = this.state.random.filter((item, index) => {
      return position != index;
    });

    this.setState({
      random: newArray
    });
  };

  render() {
    return (
      <View style={styles.mainView}>
        <Nav name={this.state.nameOfApp} />

        {/* <View style={styles.basicView}>
          <Text style={styles.basicText}>Hello World</Text>
        </View>
        <View style={styles.basicView}>
          <Text style={styles.basicText}>Hello World</Text>
        </View>

        <Generator add={this.onAddRandom} />

        <ListItem items={this.state.random} delete={this.onItemDelete} /> */}

        {/* <ScrollView style={{ width: "100%" }}> */}
        {/* <Input /> */}
        {/* <Picker />
          <ActivityIndicator size="large" color="#ff0000" /> */}
        {/* <ImageBackground
            source={{ uri: "https://picsum.photos/400/400" }}
            style={styles.mountains}
          >
            <Text>Hello</Text>
          </ImageBackground> */}
        {/* </ScrollView> */}

        <ModalComponet />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    width: "100%"
  },
  basicView: {
    backgroundColor: "green",
    width: "100%",
    marginBottom: 5
  },
  basicText: {
    fontSize: 20,
    color: "#ffffff",
    textAlign: "center",
    padding: 20
  },
  mountains: {
    width: "100%",
    height: 300,
    marginTop: 20
  }
});

export default App;
