import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Nav = props => (
  <View style={styles.nav}>
    <Text>{props.name}</Text>
  </View>
);

const styles = StyleSheet.create({
  nav: {
    backgroundColor: "red",
    alignItems: "center",
    marginTop: 30,
    padding: 20,
    fontSize: 22,
    width: "100%"
  }
});

export default Nav;
