import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";

// const generate = () => {
//   return <Button title="Add Number" onPress={() => alert("hellow")} />;
// };

const generate = props => {
  return (
    <TouchableOpacity onPress={() => props.add()} style={styles.generate}>
      <Text style={{ color: "#fff" }}>Add Number</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  generate: {
    backgroundColor: "#00bcd4",
    padding: 10,
    width: "100%",
    alignItems: "center"
  }
});

export default generate;
