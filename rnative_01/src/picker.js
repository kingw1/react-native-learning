import React, { Component } from "react";
import { StyleSheet, Text, View, Picker } from "react-native";

class PickerComponent extends Component {
  state = {
    language: "english",
    value: 50
  };

  render() {
    return (
      <View>
        <Picker
          style={{ width: "100%" }}
          selectedValue={this.state.language}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ language: itemValue })
          }
        >
          <Picker.Item label="Spanish" value="spanish" />
          <Picker.Item label="English" value="english" />
        </Picker>
      </View>
    );
  }
}

export default PickerComponent;
