import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";

import { connect } from "react-redux";
import { getArticles } from "./src/actions";
import { statement } from "@babel/template";

class App extends Component {
  componentDidMount() {
    this.props.dispatch(getArticles());
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});

function mapStateToProp(state) {
  console.log(state);
  return {
    articles: state.articles
  };
}

export default connect(mapStateToProp)(App);
